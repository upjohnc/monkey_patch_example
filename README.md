# Mocking Example

Contrived code to look at mocking functions.  Of particular interest is the mocking of an embedded function.

There are two functions that are tested.  One is `basic_function` that calls a function out of its scope.  The "external" function can easily be mocked.  The other is `embedded_function` that has a function in its scope.  The difficulty of mocking out the embedded function is expressed in [this SO post](https://stackoverflow.com/questions/27550228/can-you-patch-just-a-nested-function-with-closure-or-must-the-whole-outer-fun).


## Usage
`pipenv install` - for the basic packages

`pytest -v` - to run the tests

- believe because test is at the same level as `my_pkg` and there is a dunder init is the test directory
