def give_five():
    """
    Function simply returns a value
    :return: 5
    """
    return 5


def basic_function():
    """
    Basic function that calls an another function outside its scope
    :return: the value from `give_five` function
    """
    return give_five()


def embedded_function():
    """
    Function uses an embedded function.
    :return: the value from the embedded function `give_six`
    """
    def give_six():
        return 6

    return give_six()
