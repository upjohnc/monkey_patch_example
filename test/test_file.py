import pytest
from my_pkg import my_file


def test_regular():
    """
    Test that functions normal - simple return of `5`
    """
    function_result = my_file.basic_function()
    assert 5 == function_result


def test_fail_three(monkeypatch):
    """
    Test that the mocking of 'inner' function works
    """
    monkeypatch.setattr(my_file, 'give_five', lambda: 3)

    function_result = my_file.basic_function()

    assert 3 == function_result


@pytest.mark.xfail
def test_fail(monkeypatch):
    """
    Test that pytest shows a xfail - catches the failure correctly
    """
    monkeypatch.setattr(my_file, 'give_five', lambda: 12)

    function_result = my_file.basic_function()

    assert 5 == function_result


def test_embedded():
    """
    Test the embedded function example works correctly
    """
    function_result = my_file.embedded_function()

    assert 6 == function_result


def test_embedded_mock(monkeypatch):
    """
    Test the mocking of a function embedded in the called function

    Difficulty explained here:
    https://stackoverflow.com/questions/27550228/can-you-patch-just-a-nested-function-with-closure-or-must-the-whole-outer-fun
    """
    monkeypatch.setattr(my_file.embedded_function, 'give_six', lambda: 12)

    function_result = my_file.embedded_function()

    assert 12 == function_result
